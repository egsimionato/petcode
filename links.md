* https://dzone.com/articles/time-sensitive-this-week-in-spring-july-12th-2016
* https://dzone.com/articles/spring-5-reactive-microservices-1
* https://dzone.com/articles/microservices-with-spring?fromrel=true
* https://dzone.com/articles/learning-spring-cloud-writing?fromrel=true
* https://dzone.com/articles/dropwizard-vs-spring-boot?fromrel=true
* https://dzone.com/articles/first-step-spring-boot-and?fromrel=true
* https://dzone.com/articles/spring-boot-creating?fromrel=true
* https://dzone.com/articles/distributed-application-development?fromrel=true
* https://dzone.com/articles/why-12-factor-application-0?fromrel=true
* https://dzone.com/refcardz/restful-api-lifecycle-management?fromrel=true

* https://dzone.com/storage/assets/2542975-dzonerefcardz-microservicesinjava.pdf
* https://dzone.com/storage/assets/4960646-dzone-rc238-restfulapilifecyclemanagement.pdf

* http://www.dineshonjava.com/2017/04/spring-boot-interview-questions-and-answers.html#1




* https://travis-ci.org/spring-petclinic/spring-petclinic-microservices/

PetClinic
=========
* http://docs.spring.io/docs/petclinic.html


Microservice, Boot and Docker
-----------------------------
* https://devcenter.heroku.com/articles/deploying-spring-boot-apps-to-heroku


* https://www.3pillarglobal.com/insights/building-a-microservice-architecture-with-spring-boot-and-docker-part-i
* https://piotrminkowski.wordpress.com/2017/03/31/microservices-with-kubernetes-and-docker/
* https://piotrminkowski.wordpress.com/2017/03/20/microservices-continuous-delivery-with-docker-and-jenkins/

Boot, Data and Mongo
--------------
* http://blog.scottlogic.com/2016/11/22/spring-boot-and-mongodb.html
* http://dzone.com/articles/spring-data-mongodb-spring-boot
* [mongo] https://techannotation.wordpress.com/2017/01/30/spring-microservices-with-mongodb/
* http://www.dineshonjava.com/2016/08/spring-boot-and-mongodb-in-rest-application.html
* http://javasampleapproach.com/spring-framework-tutorial/spring-boot
* http://javasampleapproach.com/spring-framework/spring-data/spring-data-mongooperations-accessing-mongo-database


https://github.com/spring-projects/spring-data-examples
https://github.com/spring-projects/spring-data-jpa
https://github.com/spring-projects/spring-data-rest
https://github.com/olivergierke/spring-restbucks

Boot Microservices, Rabit MQ
=============================
* http://plainoldobjects.com/2014/04/01/building-microservices-with-spring-boot-part1/

MicroFull Boot
==============
* https://piotrminkowski.wordpress.com/2017/04/05/part-2-creating-microservices-monitoring-with-spring-cloud-sleuth-elk-and-zipkin/
* https://piotrminkowski.wordpress.com/2017/03/30/advanced-microservices-security-with-oauth2/
* https://dzone.com/articles/advanced-microservices-security-with-spring-and-oa


Mongo And Java
==============
* http://www.kode12.com/kode12/tag/mongodb/
* http://javasampleapproach.com/spring-framework/spring-data/spring-data-mongooperations-accessing-mongo-database
* https://www.youtube.com/watch?v=msXL2oDexqw&list=PLqq-6Pq4lTTbx8p2oCgcAQGQyqN8XeA1x
* https://www.youtube.com/watch?v=y5uImizY2eM&list=PLTaMbBKS8bHO_rElt96i8P7iXHoK8pScX
* https://www.youtube.com/watch?v=Ut7KbdnKSUM
* https://www.youtube.com/watch?v=99Nw2smMTLg&index=3&list=PLmbC-xnvykcghOSOJ1ZF6ja3aOgZAgaMO
* https://www.youtube.com/watch?v=dzdjP3CPOCs
* https://www.youtube.com/watch?v=0rdneTj0Z8U
* https://www.youtube.com/watch?v=XRREt1KB4Y8
* https://www.youtube.com/watch?v=dgVQOYEwleA
* https://www.youtube.com/watch?v=NdcnpD-GpZ4
* https://www.youtube.com/watch?v=3zrQIPwEuOs&list=PLP8n25bNfkdcGTINO2ZJXnzvMGK3nm3zY



Others
======
* http://www.kode12.com/


Repos
=====
* https://github.com/bjedrzejewski/recognitions-boot/tree/master/src/main/java/bristech
